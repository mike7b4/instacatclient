# The name of your app
TARGET = instamikeclient

# C++ sources
SOURCES += main.cpp

# C++ headers
HEADERS +=

# QML files and folders
qml.files = *.qml *.js qml/ qml/sailfishui/pages qml/sailfishui/cover js/

# The .desktop file
desktop.files = Instamikeclient.desktop

# Please do not modify the following line.
include(sailfishapplication/sailfishapplication.pri)

OTHER_FILES = rpm/Instamikeclient.yaml \
    js/instamike.js

