
#include <QApplication>
#include <QDeclarativeView>

#ifndef HARMATTAN
#include "sailfishapplication.h"
#else
#include "qmlapplication.h"
#endif

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QScopedPointer<QApplication> app(Sailfish::createApplication(argc, argv));
    QScopedPointer<QDeclarativeView> view(Sailfish::createView("qml/sailfish.qml"));
    
    Sailfish::showView(view.data());
    
    return app->exec();
}


