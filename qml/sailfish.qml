import QtQuick 1.1
import Sailfish.Silica 1.0
import "sailfishui/pages"

ApplicationWindow
{
    id: window
    initialPage: FirstPage { }
    cover: Qt.resolvedUrl("sailfishui/cover/CoverPage.qml")
}


