import QtQuick 1.1
import Sailfish.Silica 1.0
import "../../../js/instamike.js" as Remote
Page {
    id: page
    ListModel {
            id: galleryModel
    }
    Component.onCompleted:
    {
        Remote.get_small_images()
    }

    // Gridview with Sailfish Silica specific
    SilicaGridView {
        id: grid
        header: PageHeader { title: "7b4.se/instamike client" }
        cellWidth: width / 2
        cellHeight: width / 2
        anchors.fill: parent
        model: galleryModel

        // Sailfish Silica PulleyMenu on top of the grid
        PullDownMenu {
            MenuItem {
                text: "Latest first"
                onClicked: galleryModel.sortProperties = [ "-created" ]
            }
            MenuItem {
                text: "Oldest first"
                onClicked: galleryModel.sortProperties = [ "created" ]
            }
        }

        delegate: Image {
            asynchronous: true

            source:  "http://7b4.se/instamike/images/download/"+filename

            sourceSize.width: grid.cellWidth
            sourceSize.height: grid.cellHeight

            MouseArea {
                anchors.fill: parent
                onClicked: window.pageStack.push(Qt.resolvedUrl("ImagePage.qml"),
                                                 {currentIndex: index, model: grid.model} )
            }
        }
        ScrollDecorator {}
    }
}


